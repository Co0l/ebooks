﻿using EBooks.Domain;
using EBooks.Domain.Catalog;
using EBooks.Domain.Users;
using EBooks.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Infrastructure
{
    public class ContextInitializer
    {

        public static void Init()
        {
            using (var ctx = new EBookContext())
            {
                //ctx.Database.Log =  s => System.Diagnostics.Debug.WriteLine(s);
                InitPersonStatuses(ctx);
                InitClientStatuses(ctx);
                InitEmployeeStatuses(ctx);
                InitProffesions(ctx);
                InitPersons(ctx);
                InitClients(ctx);
                InitEmployees(ctx);

                InitEBookCategories(ctx);
                InitFiles(ctx);
                InitEbooks(ctx);
                InitLinks(ctx);
                InitLinkUsages(ctx);
            }
        }

        private static void InitLinkUsages(EBookContext ctx)
        {
            if (ctx.LinkUsages.Any()) return;

            var rnd = new Random();
            foreach(var link in ctx.Links)
            {
                ctx.LinkUsages.Add(new LinkUsage()
                {
                    Link = link,
                    EBookFile = link.EBook.EBookFiles.First(),
                    Ip = "194.204.20." + rnd.Next(1, 100),
                    UserAgent = "Mozilla / 5.0(Windows NT 6.1) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 46.0.2490.71 Safari / 537.36"
                });
                if (link.EBook.ProductCode == "RKAJU")
                {
                    ctx.LinkUsages.Add(new LinkUsage()
                    {
                        Link = link,
                        EBookFile = link.EBook.EBookFiles.First(),
                        Ip = "194.204.20." + rnd.Next(1, 100),
                        UserAgent = "Mozilla / 5.0(Windows NT 6.1) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 46.0.2490.71 Safari / 537.36"
                    });
                }
            }

            ctx.SaveChanges();
        }

        private static void InitLinks(EBookContext ctx)
        {
            if (ctx.Links.Any()) return;

            var teenindaja = ctx.Employees.First(x => x.Person.Email == "teenindaja@eraamatud.ee");

            ctx.Links.Add(new Link()
            {
                EBook = ctx.EBooks.First(x => x.ProductCode == "RKAJU"),
                ValidUntil = DateTime.Now.AddDays(3),
                MaxUsages = 2,
                Client = ctx.Clients.First(x => x.Person.Email == "klient@eraamatud.ee"),
                Employee = teenindaja
            });

            ctx.Links.Add(new Link()
            {
                EBook = ctx.EBooks.First(x => x.ProductCode == "RKBUU"),
                ValidUntil = DateTime.Now.AddDays(7),
                MaxUsages = 3,
                Client = ctx.Clients.First(x => x.Person.Email == "klient@eraamatud.ee"),
                Employee = teenindaja
            });

            ctx.Links.Add(new Link()
            {
                EBook = ctx.EBooks.First(x => x.ProductCode == "IKHP"),
                ValidUntil = DateTime.Now.AddDays(7),
                MaxUsages = 3,
                Client = ctx.Clients.First(x => x.Person.Email == "klient2@eraamatud.ee"),
                Employee = teenindaja
            });

            ctx.Links.Add(new Link()
            {
                EBook = ctx.EBooks.First(x => x.ProductCode == "RKBUU"),
                ValidUntil = DateTime.Now.AddDays(7),
                MaxUsages = 3,
                Client = ctx.Clients.First(x => x.Person.Email == "klient2@eraamatud.ee"),
                Employee = teenindaja
            });

            ctx.Links.Add(new Link()
            {
                EBook = ctx.EBooks.First(x => x.ProductCode == "IKTL"),
                ValidUntil = DateTime.Now.AddDays(7),
                MaxUsages = 3,
                Client = ctx.Clients.First(x => x.Person.Email == "klient2@eraamatud.ee"),
                Employee = teenindaja
            });

            ctx.Links.Add(new Link()
            {
                EBook = ctx.EBooks.First(x => x.ProductCode == "IKHP"),
                ValidUntil = DateTime.Now.AddDays(7),
                MaxUsages = 3,
                Client = ctx.Clients.First(x => x.Person.Email == "klient3@eraamatud.ee"),
                Employee = teenindaja
            });

            ctx.Links.Add(new Link()
            {
                EBook = ctx.EBooks.First(x => x.ProductCode == "RKBUU"),
                ValidUntil = DateTime.Now.AddDays(7),
                MaxUsages = 3,
                Client = ctx.Clients.First(x => x.Person.Email == "klient3@eraamatud.ee"),
                Employee = teenindaja
            });


            ctx.SaveChanges();
        }

        private static void InitEbooks(EBookContext ctx)
        {
            if (ctx.EBooks.Any()) return;

            var haldur = ctx.Employees.FirstOrDefault(x => x.EmployeeProfessions.Any(p => p.ProfessionId == (int)ProfessionType.EBookManager));

            #region ebook 1
            var ebook1 = new EBook()
            {
                Name = "Ajajuthimine",
                ProductCode = "RKAJU",
                Author = "Melissa Raffoni",
                DownloadCount = 2,
                DownloadDays = 3,
                Description = "Ajajuhtimine on midagi enamat kui lihtsalt tegemist vajavate tööde nimekirja koostamine ja oskus \"ei\" öelda. See on oskus, mis nõuab enese hindamist, planeerimist, distsipliini ning pidevat täiendamist. Tõtt-öelda võivad lohakaks minna isegi need, kes peavad end ajajuhtimise asjatundjaks. Olenemata sellest, kas alles õpid oma aega paremini kasutama või otsid uut lähenemist oma aja tõhusamale kasutamisele, on sellest raamatust abi.",
                EmployeeId = haldur.Id,
            };

            ctx.EBooks.Add(ebook1);

            ctx.EBookCategoryMappings.Add(new EBookCategoryMapping()
            {
                EBook = ebook1,
                EBookCategory = ctx.EBookCategories.First(x => x.Name == "Äriraamatud")
            });

            ctx.EBookCategoryMappings.Add(new EBookCategoryMapping()
            {
                EBook = ebook1,
                EBookCategory = ctx.EBookCategories.First(x => x.Name == "Juhtimine")
            });

            ctx.EBookFiles.Add(new EBookFile()
            {
                EBook = ebook1,
                File = ctx.Files.First(x => x.FileName == "Ajajuhtimine.epub")
            });

            #endregion

            #region ebook 2

            var ebook2 = new EBook()
            {
                Name = "Buumerang",
                SubTitle = "Reisid uude Kolmandasse Maailma",
                ProductCode = "RKBUU",
                Author = "Michael Lewis",
                Description = "Michael Lewise raamatute kaubamärk on oluliste ja keerukate teemade avamine nii silmatorkavate ja hämmastavalt veidrate karakterite kaudu, et teile tundub, nagu oleksid niisugused inimesed kindlasti välja mõeldud. Raamatu lehekülgedel kohtute taibuka mungaga, kes on leidnud viisi Kreeka kapitalismi ülekavaldamiseks, kolme päevaga Islandi panga valuutamaakleriks koolitatud kalamehega ning Iiri kinnisvaraarendajaga, kes pärast oma ettevõtte kokkukukkumist sõidab hiiglasliku ehitusmasinaga ühest Iirimaa otsast teise, et see protesti märgiks Iiri parlamendi väravate ette parkida.",
                EmployeeId = haldur.Id,
            };

            ctx.EBooks.Add(ebook2);

            ctx.EBookCategoryMappings.Add(new EBookCategoryMapping()
            {
                EBook = ebook2,
                EBookCategory = ctx.EBookCategories.First(x => x.Name == "Äriraamatud")
            });

            ctx.EBookCategoryMappings.Add(new EBookCategoryMapping()
            {
                EBook = ebook2,
                EBookCategory = ctx.EBookCategories.First(x => x.Name == "Majandus")
            });

            ctx.EBookFiles.Add(new EBookFile()
            {
                EBook = ebook2,
                File = ctx.Files.First(x => x.FileName == "Buumerang.epub")
            });

            ctx.EBookFiles.Add(new EBookFile()
            {
                EBook = ebook2,
                File = ctx.Files.First(x => x.FileName == "Buumerang.pdf")
            });

            #endregion

            #region ebook 3

            var ebook3 = new EBook()
            {
                Name = "Targad linnad",
                SubTitle = "Rünkandmed, küberputitajad ja uue utoopia otsingud",
                ProductCode = "IKTL",
                Author = "Anthony M. Townsend",
                Description = "Me elame maailmas, mida iseloomustavad linnastumine ja digitaalne kõikjalviibimine, kus mobiilsete lairibaühenduste arv ületab püsiühenduste oma, uues „asjade internetis“ domineerivad masinad ja linnades elab rohkem inimesi kui maal.",
                EmployeeId = haldur.Id,
            };

            ctx.EBooks.Add(ebook3);

            ctx.EBookCategoryMappings.Add(new EBookCategoryMapping()
            {
                EBook = ebook3,
                EBookCategory = ctx.EBookCategories.First(x => x.Name == "Ajalugu ja populaarteadus")
            });

            ctx.EBookCategoryMappings.Add(new EBookCategoryMapping()
            {
                EBook = ebook3,
                EBookCategory = ctx.EBookCategories.First(x => x.Name == "Tehnoloogia")
            });

            ctx.EBookFiles.Add(new EBookFile()
            {
                EBook = ebook3,
                File = ctx.Files.First(x => x.FileName == "Targad_linnad.epub")
            });

            ctx.EBookFiles.Add(new EBookFile()
            {
                EBook = ebook3,
                File = ctx.Files.First(x => x.FileName == "Targad_linnad_rev2.epub")
            });

            #endregion

            #region ebook 4

            var ebook4 = new EBook()
            {
                Name = "Hüpe",
                SubTitle = "Zoonoosid ja järgmine üleilmne pandeemia",
                ProductCode = "IKHP",
                Author = "David Quammen",
                Description = "Mitme teadusharu piirimail kulgevas tõsielupõnevikus viib tuntud kirjanik David Quammen lugeja reisile Austraalia võidusõiduhobuste tallidesse, Kesk-Aafrika džunglitesse, Borneo ja Bali saarele, Kagu-Aasia suurlinnade turgudele, Bangladeshi lagunenud majadesse  ja Hiina koobastesse, ajades üle maailma levivate taudide nagu SARSi, linnugripi või ka päris tavalise, kuid igal aastal erineva gripi jälgi.",
                EmployeeId = haldur.Id,
            };

            ctx.EBooks.Add(ebook4);

            ctx.EBookCategoryMappings.Add(new EBookCategoryMapping()
            {
                EBook = ebook4,
                EBookCategory = ctx.EBookCategories.First(x => x.Name == "Ajalugu ja populaarteadus")
            });

            ctx.EBookCategoryMappings.Add(new EBookCategoryMapping()
            {
                EBook = ebook4,
                EBookCategory = ctx.EBookCategories.First(x => x.Name == "Loodus")
            });

            ctx.EBookFiles.Add(new EBookFile()
            {
                EBook = ebook4,
                File = ctx.Files.First(x => x.FileName == "Hupe.epub")
            });

            #endregion

            ctx.SaveChanges();
        }

        private static void InitFiles(EBookContext ctx)
        {
            if (ctx.Files.Any()) return;

            ctx.Files.Add(new File()
            {
                FileName = "Buumerang.epub",
                Path = "c:\\inetipub\\eraamatud\\",
                Size = 246,
                Outdated = false,
            });

            ctx.Files.Add(new File()
            {
                FileName = "Buumerang.pdf",
                Path = "c:\\inetipub\\eraamatud\\",
                Size = 3482,
                Outdated = false,
            });

            ctx.Files.Add(new File()
            {
                FileName = "Ajajuhtimine.epub",
                Path = "c:\\inetipub\\eraamatud\\",
                Size = 2765,
                Outdated = false,
            });

            ctx.Files.Add(new File()
            {
                FileName = "Targad_linnad.epub",
                Path = "c:\\inetipub\\eraamatud\\",
                Size = 3893,
                Outdated = true,
            });

            ctx.Files.Add(new File()
            {
                FileName = "Targad_linnad_rev2.epub",
                Path = "c:\\inetipub\\eraamatud\\",
                Size = 6547,
                Outdated = false,
            });

            ctx.Files.Add(new File()
            {
                FileName = "Hupe.epub",
                Path = "c:\\inetipub\\eraamatud\\",
                Size = 5048,
                Outdated = false,
            });

            ctx.SaveChanges();
        }

        private static void InitEmployees(EBookContext ctx)
        {
            if (ctx.Employees.Any()) return;

            // employee 1
            var person = ctx.Persons.Where(x => x.Email == "teenindaja@eraamatud.ee").FirstOrDefault();

            var employee = new Employee()
            {
                EmployeeStatusId = (int) EmployeeStatusType.Contract, // Tööl
                PersonId = person.Id,
            };
            ctx.Employees.Add(employee);

            ctx.EmployeeProfessions.Add(new EmployeeProfession()
            {
                Employee = employee,
                ProfessionId = (int)ProfessionType.Vendor // teenindaja
            });


            // employee 2
            var person2 = ctx.Persons.Where(x => x.Email == "vitali.mihhejev@eraamatud.ee").FirstOrDefault();

            var employee2 = new Employee()
            {
                EmployeeStatusId = (int)EmployeeStatusType.Contract, // Tööl
                PersonId = person2.Id,
            };
            ctx.Employees.Add(employee2);

            ctx.EmployeeProfessions.Add(new EmployeeProfession()
            {
                Employee = employee2,
                ProfessionId = (int)ProfessionType.Vendor // teenindaja
            });
            ctx.EmployeeProfessions.Add(new EmployeeProfession()
            {
                Employee = employee2,
                ProfessionId = (int)ProfessionType.EBookManager // haldur
            });

            // employee 2
            var person3 = ctx.Persons.Where(x => x.Email == "haldur@eraamatud.ee").FirstOrDefault();

            var employee3 = new Employee()
            {
                EmployeeStatusId = (int)EmployeeStatusType.Contract, // Tööl
                PersonId = person3.Id,
            };
            ctx.Employees.Add(employee3);

            ctx.EmployeeProfessions.Add(new EmployeeProfession()
            {
                Employee = employee3,
                ProfessionId = (int)ProfessionType.EBookManager // haldur
            });

            ctx.SaveChanges();
        }

        private static void InitClients(EBookContext ctx)
        {
            if (ctx.Clients.Any()) return;

            ctx.Clients.Add(new Client()
            {
                ClientStatusId = (int)ClientStatusType.Active,
                Person = ctx.Persons.First(x => x.Email == "klient@eraamatud.ee"),
                RulesAccepted = true
            });

            ctx.Clients.Add(new Client()
            {
                ClientStatusId = (int)ClientStatusType.Active,
                Person = ctx.Persons.First(x => x.Email == "klient2@eraamatud.ee"),
                RulesAccepted = true
            });

            ctx.Clients.Add(new Client()
            {
                ClientStatusId = (int)ClientStatusType.Blocked,
                Person = ctx.Persons.First(x => x.Email == "klient3@eraamatud.ee"),
                RulesAccepted = true
            });

            ctx.Clients.Add(new Client()
            {
                ClientStatusId = (int)ClientStatusType.Inactive,
                Person = ctx.Persons.First(x => x.Email == "klient4@eraamatud.ee"),
                RulesAccepted = true
            });

            ctx.SaveChanges();
        }

        private static void InitPersons(EBookContext ctx)
        {
            if (ctx.Persons.Any()) return;


            var salt = CommonHelper.CreateSaltKey();
            ctx.Persons.Add(new Person()
            {
                Email = "vitali.mihhejev@eraamatud.ee",
                Firstname = "Vitali",
                Lastname = "Mihhejev",
                Phone = "53738728",
                Salt = salt,
                Password = CommonHelper.CreatePasswordHash("test", salt),
                PersonStatusId = (int)PersonStatusType.Alive,
                RegisteredOn = DateTime.Now
            });

            salt = CommonHelper.CreateSaltKey();
            ctx.Persons.Add(new Person()
            {
                Email = "haldur@eraamatud.ee",
                Firstname = "Haldur",
                Lastname = "Testija",
                Phone = "55554444",
                Salt = salt,
                Password = CommonHelper.CreatePasswordHash("test", salt),
                PersonStatusId = (int)PersonStatusType.Alive,
                RegisteredOn = DateTime.Now
            });

            salt = CommonHelper.CreateSaltKey();
            ctx.Persons.Add(new Person()
            {
                Email = "klient@eraamatud.ee",
                Firstname = "Ave",
                Lastname = "Testija",
                Phone = "55551111",
                Salt = salt,
                Password = CommonHelper.CreatePasswordHash("test", salt),
                PersonStatusId = (int)PersonStatusType.Alive,
                RegisteredOn = DateTime.Now
            });

            salt = CommonHelper.CreateSaltKey();
            ctx.Persons.Add(new Person()
            {
                Email = "klient2@eraamatud.ee",
                Firstname = "Margot",
                Lastname = "Testija",
                Phone = "55552222",
                Salt = salt,
                Password = CommonHelper.CreatePasswordHash("test", salt),
                PersonStatusId = (int)PersonStatusType.Alive,
                RegisteredOn = DateTime.Now
            });

            salt = CommonHelper.CreateSaltKey();
            ctx.Persons.Add(new Person()
            {
                Email = "klient3@eraamatud.ee",
                Firstname = "Tanel",
                Lastname = "Testija",
                Phone = "55553333",
                Salt = salt,
                Password = CommonHelper.CreatePasswordHash("test", salt),
                PersonStatusId = (int)PersonStatusType.Alive,
                RegisteredOn = DateTime.Now
            });

            salt = CommonHelper.CreateSaltKey();
            ctx.Persons.Add(new Person()
            {
                Email = "klient4@eraamatud.ee",
                Firstname = "Rasmus",
                Lastname = "Testija",
                Phone = "55554444",
                Salt = salt,
                Password = CommonHelper.CreatePasswordHash("test", salt),
                PersonStatusId = (int)PersonStatusType.Alive,
                RegisteredOn = DateTime.Now
            });

            salt = CommonHelper.CreateSaltKey();
            ctx.Persons.Add(new Person()
            {
                Email = "teenindaja@eraamatud.ee",
                Firstname = "Teenindaja",
                Lastname = "Testija",
                Phone = "55553333",
                Salt = salt,
                Password = CommonHelper.CreatePasswordHash("test", salt),
                PersonStatusId = (int)PersonStatusType.Alive,
                RegisteredOn = DateTime.Now
            });

            ctx.SaveChanges();
        }

        private static void InitEmployeeStatuses(EBookContext ctx)
        {
            if (ctx.EmployeeStatuses.Any()) return;

            ctx.EmployeeStatuses.Add(new EmployeeStatus()
            {
                Id = (int) EmployeeStatusType.TestPeriod,
                Name = "Katseajal"
            });

            ctx.EmployeeStatuses.Add(new EmployeeStatus()
            {
                Id = (int)EmployeeStatusType.Contract,
                Name = "Tööl"
            });

            ctx.EmployeeStatuses.Add(new EmployeeStatus()
            {
                Id = (int)EmployeeStatusType.OnVacation,
                Name = "Puhkusel"
            });

            ctx.EmployeeStatuses.Add(new EmployeeStatus()
            {
                Id = (int)EmployeeStatusType.Sick,
                Name = "Haiguslehel"
            });

            ctx.EmployeeStatuses.Add(new EmployeeStatus()
            {
                Id = (int)EmployeeStatusType.ContractCanceled,
                Name = "Töösuhe peatatud"
            });

            ctx.EmployeeStatuses.Add(new EmployeeStatus()
            {
                Id = (int)EmployeeStatusType.ContractCanceledByPerson,
                Name = "Töösuhe lõpetatud omal soovil"
            });

            ctx.EmployeeStatuses.Add(new EmployeeStatus()
            {
                Id = (int)EmployeeStatusType.Fired,
                Name = "Vallandatud"
            });

            ctx.SaveChanges();
        }

        private static void InitProffesions(EBookContext ctx)
        {
            if (ctx.Professions.Any()) return;

            ctx.Professions.Add(new Profession()
            {
                Id = (int)ProfessionType.Administrator,
                Name = "Juhataja"
            });

            ctx.Professions.Add(new Profession()
            {
                Id = (int)ProfessionType.EBookManager,
                Name = "E-raamatute haldur"
            });

            ctx.Professions.Add(new Profession()
            {
                Id = (int)ProfessionType.Vendor,
                Name = "Teenindaja"
            });

            ctx.SaveChanges();
        }

        private static void InitEBookCategories(EBookContext ctx)
        {
            if (ctx.EBookCategories.Any()) return;

            ctx.EBookCategories.Add(new EBookCategory()
            {
                Id = 1,
                Name = "Äriraamatud"
            });

            ctx.EBookCategories.Add(new EBookCategory()
            {
                Id = 2,
                Name = "Majandus",
                Description = "majandus kategooria"
            });

            ctx.EBookCategories.Add(new EBookCategory()
            {
                Id = 3,
                Name = "Juhtimine"
            });

            ctx.EBookCategories.Add(new EBookCategory()
            {
                Id = 4,
                Name = "Ajalugu ja populaarteadus"
            });

            ctx.EBookCategories.Add(new EBookCategory()
            {
                Id = 5,
                Name = "Tehnoloogia"
            });

            ctx.EBookCategories.Add(new EBookCategory()
            {
                Id = 6,
                Name = "Loodus"
            });

            ctx.SaveChanges();
        }

        private static void InitClientStatuses(EBookContext ctx)
        {
            if (ctx.ClientStatuses.Any()) return;

            ctx.ClientStatuses.Add(new ClientStatus()
            {
                Id = (int)ClientStatusType.Inactive,
                Name = "Mitteaktiivne",
                Description = "E-mail ei ole kinnitatud"
            });

            ctx.ClientStatuses.Add(new ClientStatus()
            {
                Id = (int)ClientStatusType.Active,
                Name = "Aktiivne"
            });

            ctx.ClientStatuses.Add(new ClientStatus()
            {
                Id = (int)ClientStatusType.Blocked,
                Name = "Blokeeritud",
                Description = "Mustas nimekirjas"
            });

            ctx.SaveChanges();
        }

        private static void InitPersonStatuses(EBookContext ctx)
        {
            if (ctx.PersonStatuses.Any()) return;

            ctx.PersonStatuses.Add(new PersonStatus()
            {
                Id = (int)PersonStatusType.Alive,
                Name = "Elus"
            });

            ctx.PersonStatuses.Add(new PersonStatus()
            {
                Id = (int)PersonStatusType.Dead,
                Name = "Suurnud"
            });

            ctx.SaveChanges();
        }
    }
}