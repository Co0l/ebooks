﻿using EBooks.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Catalog
{
    public class LinkMap : EntityTypeConfiguration<Link>
    {

        public LinkMap()
        {
            this.ToTable("T167023_LINK");

            this.HasKey(x => x.Id);

            this.Property(x => x.MaxUsages).IsRequired().HasColumnName("ALLALAADIMISE_ARV");
            this.Property(x => x.ValidUntil).IsRequired().HasColumnName("KEHTIV_KUNI");
            this.Property(x => x.CreatedOn).IsRequired().HasColumnName("LOODUD");

            this.HasRequired(x => x.EBook)
                .WithMany(b => b.Links)
                .HasForeignKey(x => x.EBookId);

            this.HasMany(x => x.LinkUsages)
                .WithRequired(lu => lu.Link)
                .HasForeignKey(lu => lu.LinkId);

            this.HasRequired(x => x.Client)
                .WithMany()
                .HasForeignKey(x => x.ClientId);

            this.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId);

            this.Property(x => x.EBookId).HasColumnName("ERAAMAT_ID");
            this.Property(x => x.Id).HasColumnName("LINK_ID");
            this.Property(x => x.ClientId).HasColumnName("KLIENT_ID");
            this.Property(x => x.EmployeeId).HasColumnName("TOOTAJA_ID");
        }
    }
}