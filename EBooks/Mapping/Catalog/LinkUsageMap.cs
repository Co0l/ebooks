﻿using EBooks.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Catalog
{
    public class LinkUsageMap : EntityTypeConfiguration<LinkUsage>
    {

        public LinkUsageMap()
        {
            this.ToTable("T167023_ALLALAADIMINE");

            this.HasKey(x => x.Id);

            this.Property(x => x.UsedOn).IsRequired().HasColumnName("LOODUD");
            this.Property(x => x.UserAgent).IsRequired().HasMaxLength(254).HasColumnName("KASUTAJA_AGENT");
            this.Property(x => x.Ip).IsRequired().HasMaxLength(40).HasColumnName("IP");

            this.HasRequired(x => x.Link)
                .WithMany(l => l.LinkUsages)
                .HasForeignKey(x => x.LinkId)
                .WillCascadeOnDelete(true);

            this.HasRequired(x => x.EBookFile)
                .WithMany()
                .HasForeignKey(ef => ef.EBookFileId)
                .WillCascadeOnDelete(true);

            this.Property(x => x.Id).HasColumnName("ALLALAADIMINE_ID");
            this.Property(x => x.LinkId).HasColumnName("LINK_ID");
            this.Property(x => x.EBookFileId).HasColumnName("ERAAMATU_FAIL_ID");
        }
    }
}