﻿using EBooks.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Catalog
{
    public class EBookMap : EntityTypeConfiguration<EBook>
    {

        public EBookMap()
        {
            this.ToTable("T167023_ERAAMAT");

            this.HasKey(x => x.Id);
            this.Property(x => x.ProductCode).IsRequired().HasMaxLength(20).HasColumnName("ERAAMATU_KOOD").IsUnicode(false);
            this.Property(x => x.Name).IsRequired().HasMaxLength(254).HasColumnName("PEALKIRI");
            this.Property(x => x.SubTitle).IsOptional().HasMaxLength(254).HasColumnName("ALAPEALKIRI");
            this.Property(x => x.Description).IsOptional().HasMaxLength(2000).HasColumnName("KIRJELDUS");
            this.Property(x => x.Author).IsOptional().HasMaxLength(254).HasColumnName("AUTOR");
            this.Property(x => x.DownloadCount).IsRequired().HasColumnName("ALLALAADIMISE_ARV");
            this.Property(x => x.DownloadDays).IsRequired().HasColumnName("ALLALAADIMISE_KESTUS");
            this.Property(x => x.CreatedOn).IsRequired().HasColumnName("LOODUD");

            this.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId);

            this.HasMany(x => x.Links).WithRequired(l => l.EBook).HasForeignKey(l => l.EBookId);
            this.HasMany(x => x.EBookFiles).WithRequired(f => f.EBook).HasForeignKey(f => f.EBookId);
            this.HasMany(x => x.EBookCategoryMappings).WithRequired(c => c.EBook).HasForeignKey(c => c.EBookId);

            this.Property(x => x.Id).HasColumnName("ERAAMAT_ID");
            this.Property(x => x.EmployeeId).HasColumnName("TOOTAJA_ID");
        }
    }
}