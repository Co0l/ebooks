﻿using EBooks.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Catalog
{
    public class EBookCategoryMap : EntityTypeConfiguration<EBookCategory>
    {

        public EBookCategoryMap()
        {
            this.ToTable("T167023_ERAAMATU_KATEGOORIA");

            this.HasKey(x => x.Id);
            this.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).HasColumnName("ERAAMATU_KATEGOORIA_ID");
            this.Property(x => x.Name).IsRequired().HasMaxLength(254).HasColumnName("NIMI");
            this.Property(x => x.Description).IsOptional().HasMaxLength(1000).HasColumnName("KIRJELDUS");

        }
    }
}