﻿using EBooks.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Catalog
{
    public class EBookCategoryMappingMap : EntityTypeConfiguration<EBookCategoryMapping>
    {

        public EBookCategoryMappingMap()
        {
            this.ToTable("T167023_ERAAMATU_KATEGOORIA_R");

            this.HasKey(x => x.Id);

            this.HasRequired(x => x.EBook)
                .WithMany(b => b.EBookCategoryMappings)
                .HasForeignKey(x => x.EBookId);

            this.HasRequired(x => x.EBookCategory)
                .WithMany()
                .HasForeignKey(x => x.EBookCategoryId);

            this.Property(x => x.Id).HasColumnName("ERAAMATU_KATEGOORIA_R_ID");
            this.Property(x => x.EBookId).HasColumnName("ERAAMAT_ID");
            this.Property(x => x.EBookCategoryId).HasColumnName("ERAAMATU_KATEGOORIA_ID");
        }

    }
}