﻿using EBooks.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Catalog
{
    public class FileMap : EntityTypeConfiguration<File>
    {

        public FileMap() 
        {
            this.ToTable("T167023_FAIL");

            this.HasKey(x => x.Id);

            this.Property(x => x.FileName).IsRequired().HasMaxLength(200).HasColumnName("FAILI_NIMI").IsUnicode();
            this.Property(x => x.Path).IsRequired().HasMaxLength(400).HasColumnName("KAUSTA_TEE");
            this.Property(x => x.Size).IsOptional().HasColumnName("SUURUS");
            this.Property(x => x.Outdated).IsRequired().HasColumnName("ON_AEGUNUD");
                
            this.Property(x => x.Id).HasColumnName("FAIL_ID");

        }
    }
}