﻿using EBooks.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Catalog
{
    public class EBookFileMap : EntityTypeConfiguration<EBookFile>
    {

        public EBookFileMap()
        {
            this.ToTable("T167023_ERAAMATU_FAIL");

            this.HasKey(x => x.Id);

            this.HasRequired(x => x.EBook)
                .WithMany(b => b.EBookFiles)
                .HasForeignKey(x => x.EBookId);

            this.HasRequired(x => x.File)
                .WithMany()
                .HasForeignKey(x => x.FileId);

            this.Property(x => x.Id).HasColumnName("ERAAMATU_FAIL_ID");
            this.Property(x => x.EBookId).HasColumnName("ERAAMAT_ID");
            this.Property(x => x.FileId).HasColumnName("FAIL_ID");

        }
    }
}