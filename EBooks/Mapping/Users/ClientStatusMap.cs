﻿using EBooks.Domain.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Users
{
    public class ClientStatusMap : EntityTypeConfiguration<ClientStatus>
    {

        public ClientStatusMap()
        {
            this.ToTable("T167023_KLIENDI_STAATUS");

            this.HasKey(x => x.Id);

            this.Property(x => x.Name).IsRequired().HasMaxLength(254).HasColumnName("NIMI");
            this.Property(x => x.Description).IsOptional().HasMaxLength(1000).HasColumnName("KIRJELDUS");

            this.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).HasColumnName("KLIENDI_STAATUS_ID");

        }
    }
}