﻿using EBooks.Domain.Users;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Users
{
    public class ClientMap : EntityTypeConfiguration<Client>
    {

        public ClientMap()
        {
            this.ToTable("T167023_KLIENT");

            this.HasKey(x => x.Id);

            this.Property(x => x.RulesAccepted).HasColumnName("ON_NOUS_OTSETURUNDUSEGA").IsRequired();

            this.HasRequired(x => x.Person)
                .WithMany()
                .HasForeignKey(x => x.PersonId);

            this.HasRequired(x => x.ClientStatus)
                .WithMany()
                .HasForeignKey(x => x.ClientStatusId);

            this.Property(x => x.Id).HasColumnName("KLIENT_ID");
            this.Property(x => x.ClientStatusId).HasColumnName("KLIENDI_STAATUS_ID");
            this.Property(x => x.PersonId).HasColumnName("ISIK_ID");

        }
    }
}