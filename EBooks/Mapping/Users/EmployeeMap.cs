﻿using EBooks.Domain.Users;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Users
{
    public class EmployeeMap : EntityTypeConfiguration<Employee>
    {

        public EmployeeMap()
        {
            this.ToTable("T167023_TOOTAJA");

            this.HasKey(x => x.Id);

            this.HasRequired(x => x.Person)
                .WithMany()
                .HasForeignKey(x => x.PersonId);

            this.HasMany(x => x.EmployeeProfessions)
                .WithRequired(x => x.Employee)
                .HasForeignKey(x => x.EmployeeId);

            this.HasRequired(x => x.EmployeeStatus)
                .WithMany()
                .HasForeignKey(x => x.EmployeeStatusId);

            this.Property(x => x.Id).HasColumnName("TOOTAJA_ID");
            this.Property(x => x.PersonId).HasColumnName("ISIK_ID");
            this.Property(x => x.EmployeeStatusId).HasColumnName("TOOTAJA_STAATUS_ID");

        }
    }
}