﻿using EBooks.Domain.Users;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Users
{
    public class EmployeeProfessionMap : EntityTypeConfiguration<EmployeeProfession>
    {

        public EmployeeProfessionMap()
        {
            this.ToTable("T167023_TOOTAJA_AMET");

            this.HasKey(x => x.Id);

            this.HasRequired(x => x.Employee)
                .WithMany(b => b.EmployeeProfessions)
                .HasForeignKey(x => x.EmployeeId);

            this.HasRequired(x => x.Profession)
                .WithMany()
                .HasForeignKey(x => x.ProfessionId);

            this.Property(x => x.Id).HasColumnName("TOOTAJA_AMET_ID");
            this.Property(x => x.EmployeeId).HasColumnName("TOOTAJA_ID");
            this.Property(x => x.ProfessionId).HasColumnName("AMET_ID");

        }
    }
}