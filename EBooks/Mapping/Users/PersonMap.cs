﻿using EBooks.Domain.Users;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.Users
{
    public class PersonMap : EntityTypeConfiguration<Person>
    {

        public PersonMap()
        {
            this.ToTable("T167023_ISIK");

            this.HasKey(x => x.Id);

            this.Property(x => x.Email).IsRequired().HasMaxLength(254).HasColumnName("EMAIL").IsUnicode(false);
            this.Property(x => x.Password).IsRequired().HasMaxLength(64).HasColumnName("PAROOL");
            this.Property(x => x.Salt).IsRequired().HasMaxLength(64).HasColumnName("SOOL");
            this.Property(x => x.Firstname).IsRequired().HasMaxLength(254).HasColumnName("EESNIMI");
            this.Property(x => x.Lastname).IsRequired().HasMaxLength(254).HasColumnName("PERENIMI");
            this.Property(x => x.Phone).IsRequired().HasMaxLength(100).HasColumnName("TELEFONI_NUMBER");
            this.Property(x => x.Address).IsOptional().HasMaxLength(1000).HasColumnName("ELUKOHT");
            this.Property(x => x.RegisteredOn).IsRequired().HasColumnName("REGISTREERITUD");

            this.HasRequired(x => x.PersonStatus)
                .WithMany()
                .HasForeignKey(x => x.PersonStatusId);

            this.Property(x => x.PersonStatusId).HasColumnName("ISIKU_STAATUS_ID");
            this.Property(x => x.Id).HasColumnName("ISIK_ID");
        }
    }
}