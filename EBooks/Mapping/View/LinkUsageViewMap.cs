﻿using EBooks.Domain.View;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.View
{
    public class LinkUsageViewMap : EntityTypeConfiguration<LinkUsageView>
    {

        public LinkUsageViewMap()
        {
            ToTable("T167023_ALLALAADIMINE_VIEW");

            Property(x => x.Id).HasColumnName("ALLALAADIMINE_ID");
            Property(x => x.LinkId).HasColumnName("LINK_ID");

            Property(x => x.FailName).IsRequired().HasMaxLength(200).HasColumnName("FAILI_NIMI").IsUnicode();
            Property(x => x.Ip).IsRequired().HasMaxLength(40).HasColumnName("IP");
            Property(x => x.UsedOn).IsRequired().HasColumnName("LOODUD");
            Property(x => x.UserAgent).IsRequired().HasMaxLength(254).HasColumnName("KASUTAJA_AGENT").IsUnicode();
        }
    }
}