﻿using EBooks.Domain.View;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.View
{
    public class EBookViewMap : EntityTypeConfiguration<EBookView>
    {

        public EBookViewMap()
        {
            ToTable("T167023_ERAAMAT_VIEW");

            Property(x => x.Id).IsRequired().HasColumnName("ERAAMAT_ID");
            Property(x => x.ProductCode).IsRequired().HasMaxLength(20).HasColumnName("ERAAMATU_KOOD").IsUnicode(false);
            Property(x => x.Name).IsRequired().HasMaxLength(254).HasColumnName("PEALKIRI");
            Property(x => x.Author).IsOptional().HasMaxLength(254).HasColumnName("AUTOR");
            Property(x => x.DownloadCount).IsRequired().HasColumnName("ALLALAADIMISE_ARV");
            Property(x => x.DownloadDays).IsRequired().HasColumnName("ALLALAADIMISE_KESTUS");
            Property(x => x.CreatedOn).IsRequired().HasColumnName("LOODUD");

            Property(x => x.NumberOfDownloads).IsRequired().HasColumnName("ALLALAADIMISI_KOKKU");
            Property(x => x.NumberOfLinks).IsRequired().HasColumnName("LINKIDE_ARV");
        }
    }
}