﻿using EBooks.Domain.View;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace EBooks.Mapping.View
{
    public class LinkViewMap : EntityTypeConfiguration<LinkView>
    {

        public LinkViewMap()
        {
            ToTable("T167023_LINK_VIEW");

            Property(x => x.Id).HasColumnName("LINK_ID");
            Property(x => x.EBookId).HasColumnName("ERAAMAT_ID");

            Property(x => x.EBookCode).IsRequired().HasMaxLength(20).HasColumnName("ERAAMATU_KOOD").IsUnicode(false);
            Property(x => x.EBookName).IsRequired().HasMaxLength(254).HasColumnName("PEALKIRI");

            Property(x => x.ClientEmail).IsRequired().HasMaxLength(254).HasColumnName("EMAIL").IsUnicode(false);

            Property(x => x.MaxUsages).IsRequired().HasColumnName("ALLALAADIMISE_ARV");
            Property(x => x.LeftUsages).IsRequired().HasColumnName("SAADAVAL");
            Property(x => x.ValidUntil).IsRequired().HasColumnName("KEHTIV_KUNI");

        }
    }
}