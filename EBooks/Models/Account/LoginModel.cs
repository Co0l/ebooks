﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBooks.Models.Account
{
    public class LoginModel 
    {
        [Display(Name = "E-post")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Palun kontrollige e-posti aadressi formaati")]
        [EmailAddress(ErrorMessage = "Palun kontrollige e-posti aadressi formaati")]
        [Required(ErrorMessage = "E-post on kohustuslik")]
        public string Email { get; set; }

        [Display(Name = "Parool")]
        [AllowHtml]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Parool on kohustuslik")]
        public string Password { get; set; }

    }
}