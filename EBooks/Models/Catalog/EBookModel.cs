﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBooks.Models.Catalog
{
    public class EBookModel : EntityModel
    {

        [Required]
        [MaxLength(20)]
        [Display(Name = "E-raamatu kood")]
        public string ProductCode { get; set; }

        [Required]
        [MaxLength(254)]
        [Display(Name = "Pealkiri")]
        public string Name { get; set; }

        [MaxLength(254)]
        [Display(Name = "Alapealkiri")]
        public string SubTitle { get; set; }

        [Display(Name = "Kirjeldus")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Allaladimise arv")]
        [Range(1, int.MaxValue)]
        public int DownloadCount { get; set; } = 3;

        [Required]
        [Display(Name = "Allaladimise kestus")]
        [Range(1, int.MaxValue)]
        public int DownloadDays { get; set; } = 7;

        public int NumberOfDownloads { get; set; }
        public int NumberOfLinks { get; set; }

        [Required]
        [Display(Name = "Loodud")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOn { get; set; } = DateTime.Now;

        public string Author { get; set; }

    }
}