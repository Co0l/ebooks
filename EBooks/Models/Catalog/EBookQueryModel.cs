﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Models.Catalog
{
    public class EBookQueryModel
    {

        public string SearchByProductCode { get; set; }
        public string SearchByName { get; set; }
        public string SearchByAuthor { get; set; }

        public List<EBookModel> EBooks { get; set; }

    }
}