﻿using EBooks.Domain.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBooks.Models.Catalog
{
    public class PersonModel : EntityModel
    {

        public int PersonStatusId { get; set; }

        [Display(Name = "E-post")]
        public string Email { get; set; }

        [Display(Name = "Eesnimi")]
        public string Firstname { get; set; }

        [Display(Name = "Perenimi")]
        public string Lastname { get; set; }

        [Display(Name = "Telefoni number")]
        public string Phone { get; set; }

        [Display(Name = "Elukoht")]
        public string Address { get; set; }

        [Display(Name = "Registreerimise aeg")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Editable(false)]
        public DateTime RegisteredOn { get; set; } = DateTime.Now;

        [Display(Name = "Staatus")]
        public PersonStatus PersonStatus { get; set; }
    }
}