﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBooks.Models.Catalog
{
    public class LinkUsageListModel : EntityModel
    {
        [Display(Name = "Kasutatud")]
        public DateTime UsedOn { get; set; }

        [Display(Name = "IP")]
        public string Ip { get; set; }

        [Display(Name = "Kasutaja agent")]
        public string UserAgent { get; set; }

        [Display(Name = "Faili nimi")]
        public string FileName { get; set; }

    }
}