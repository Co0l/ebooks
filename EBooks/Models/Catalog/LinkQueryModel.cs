﻿using EBooks.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBooks.Models.Catalog
{
    public class LinkQueryModel
    {
        [Display(Name = "Kliendi e-post")]
        public string SearchByEmail { get; set; }

        [Display(Name = "E-raamat")]
        public int SearchByEBookId { get; set; }

        [Display(Name = "Aktiivne")]
        public int SelectedActiveId { get; set; }

        public List<LinkListModel> Links { get; set; }

        public IList<SelectListItem> AvailableEBooks { get; set; }

        public IList<SelectListItem> AvailableActiveTypes { get; set; }

        public LinkQueryModel()
        {
            AvailableActiveTypes = new List<SelectListItem>();
            AvailableActiveTypes.Add(new SelectListItem() { Text = "Igasugune", Value = ((int)ActiveType.Any).ToString() });
            AvailableActiveTypes.Add(new SelectListItem() { Text = "Jah", Value = ((int)ActiveType.Active).ToString() });
            AvailableActiveTypes.Add(new SelectListItem() { Text = "Ei", Value = ((int)ActiveType.NotActive).ToString() });
        }

    }
}