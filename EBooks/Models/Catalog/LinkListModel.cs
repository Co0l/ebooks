﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EBooks.Models.Catalog
{
    public class LinkListModel : EntityModel
    {

        public string EBookCode { get; set; }

        public string EBookName { get; set; }

        public string ClientEmail { get; set; }
        
        public int MaxUsages { get; set; }

        public int LeftUsages { get; set; }

        public int Usages { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}")]
        public DateTime ValidUntil { get; set; }

        public bool IsActive
        {
            get
            {
                return ValidUntil > DateTime.Now && LeftUsages > 0;
            }
        }

    }
}