﻿using EBooks.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBooks.Models.Catalog
{
    public class LinkModel : EntityModel
    {
        [Display(Name = "Klient")]
        [Required(ErrorMessage = "Klienti valimine on kohustuslik")]
        [Range(1, int.MaxValue, ErrorMessage = "Klienti valimine on kohustuslik")]
        public int ClientId { get; set; }

        [Display(Name = "Töötaja")]
        public int EmployeeId { get; set; }

        [Display(Name = "E-raamat")]
        [Required(ErrorMessage = "E-raamatu valimine on kohustuslik")]
        [Range(1, int.MaxValue, ErrorMessage = "E-raamatu valimine on kohustuslik")]
        public int EBookId { get; set; }

        [Display(Name = "Allalaadimise arv")]
        [Range(1, 100, ErrorMessage = "Peab olema vahemikus 1 kuni 100")]
        [Required(ErrorMessage = "Allalaadimise arv on kohustuslik")]
        public int MaxUsages { get; set; } = 3;

        [Display(Name = "Kehtivuse kuupäev")]
        [IsDateGreater("CreatedOn", ErrorMessage = "Kehtivuse kuupäev peab olema suurem kui lingi registreerimise kuupäev")]
        [Required(ErrorMessage = "Kehtivuse kuupäev on kohustuslik")]
        public DateTime ValidUntil { get; set; } 

        [Display(Name = "Loodud")]
        public DateTime CreatedOn { get; set; }

        public EmployeeModel Employee { get; set; }

        public ClientModel Client { get; set; }

        public EBookModel EBook { get; set; }

        public List<LinkUsageListModel> LinkUsages { get; set; }

        public IList<SelectListItem> AvailableClients { get; set; }

        public IList<SelectListItem> AvailableEBooks { get; set; }

        public string SuccessMessage { get; set; }

        public string EmployeeName
        {
            get
            {
                return $"{Employee.Firstname} {Employee.Lastname}, {Employee.Email}";
            }
        }

        public LinkModel()
        {
            ValidUntil = DateTime.Now.AddDays(7);
            CreatedOn = DateTime.Now;
        }       

    }
}