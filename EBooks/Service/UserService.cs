﻿using EBooks.Domain;
using EBooks.Domain.Users;
using EBooks.Enums;
using EBooks.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Service
{
    public class UserService : Repository<Person>
    {

        private readonly EmployeeService _employeeService;

        public UserService(EBookContext context) : base(context)
        {
            _employeeService = new EmployeeService(context);
        }

        public Person ValidateUser(string email, string password)
        {
            var user = Table.Where(x => x.Email == email).FirstOrDefault();
            if (user != null)
            {
                if (CommonHelper.CreatePasswordHash(password, user.Salt) == user.Password)
                    return user;
            }
            return null;
        }

        public bool IsActiveVendor(Person person)
        {
            var employee = _employeeService.Table.Where(x => x.PersonId == person.Id).FirstOrDefault();
            if (employee == null) return false;
            return employee.IsVendor() && employee.IsActive();
        }

    }

    public class EmployeeService : Repository<Employee>
    {
        public EmployeeService(EBookContext context) : base(context)
        {

        }

    }

    public static class UserExtensions
    {
        public static bool IsActive(this Person person)
        {
            return (person.PersonStatus.Id == (int)PersonStatusType.Alive);
        }

        public static bool IsActive(this Employee employee)
        {
            var activeStatuses = new int[] { (int)EmployeeStatusType.Contract, (int)EmployeeStatusType.OnVacation, (int)EmployeeStatusType.TestPeriod, (int)EmployeeStatusType.Sick };
            return employee.Person.IsActive() && activeStatuses.Contains(employee.EmployeeStatus.Id);
        }

        public static bool IsVendor(this Employee employee)
        {
            return employee.EmployeeProfessions.Any(x => x.Profession.Id == (int)ProfessionType.Vendor);
        }
    }
}