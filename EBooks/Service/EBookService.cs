﻿using EBooks.Domain;
using EBooks.Domain.Catalog;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Web.WebPages;
using System.Linq;

namespace EBooks.Service
{
    public class EBookService : Repository<EBook>
    {

        public EBookService(EBookContext context) : base (context)
        {

        }

        public List<EBook> Search(string productCode = null, string name = null, string author = null)
        {
            var query = this.Table;

            if (!string.IsNullOrWhiteSpace(productCode))
            {
                query = query.Where(x => x.ProductCode.ToLower().Contains(productCode.ToLower()));   
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query.Where(x => x.Name.ToLower().Contains(name.ToLower()));
            }

            if (!string.IsNullOrWhiteSpace(author))
            {
                query = query.Where(x => x.Author.ToLower().Contains(author.ToLower()));
            }

            return query.OrderBy(x => x.Id).ToList();
        }

    }
}