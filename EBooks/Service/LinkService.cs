﻿using EBooks.Domain;
using EBooks.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EBooks.Models.Catalog;
using EBooks.Enums;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using EBooks.Domain.StoredProcedure;
using EBooks.Domain.View;

namespace EBooks.Service
{
    public class LinkService : Repository<Link>
    {

        public LinkService(EBookContext context) : base(context)
        {

        }

        public void Add(Link link)
        {
            var clientId = new OracleParameter("p_klient_id", OracleDbType.Int32, 10, link.ClientId, ParameterDirection.Input);
            var employeeId = new OracleParameter("p_tootaja_id", OracleDbType.Int32, 10, link.EmployeeId, ParameterDirection.Input);
            var ebookId = new OracleParameter("p_eraamatu_id", OracleDbType.Int32, 10, link.EBookId, ParameterDirection.Input);
            var maxUsages = new OracleParameter("p_allalaadimise_arv", OracleDbType.Int32, 10, link.MaxUsages, ParameterDirection.Input);
            var validUntil = new OracleParameter("p_kehtiv_kuni", OracleDbType.TimeStamp, 100, link.ValidUntil, ParameterDirection.Input);

            _context.Database.ExecuteSqlCommand(
                "BEGIN C##TUD3.T167023.LISA_LINGI(:p_klient_id, :p_tootaja_id, :p_eraamatu_id, :p_allalaadimise_arv, :p_kehtiv_kuni); end;",
                clientId, employeeId, ebookId, maxUsages, validUntil);
        }

        public void Remove(int linkId)
        {
            var linkIdParam = new OracleParameter("p_link_id", OracleDbType.Int32, 10, linkId, ParameterDirection.Input);

            _context.Database.ExecuteSqlCommand("BEGIN C##TUD3.T167023.EEMALDA_LINGI(:p_link_id); end;", linkIdParam);
        }

        //public List<Link> Search(string email, int ebookId, ActiveType active)
        //{
        //    var query = this.Table;

        //    if (ebookId > 0)
        //    {
        //        query = query.Where(x => x.EBookId == ebookId);
        //    }

        //    if (!string.IsNullOrWhiteSpace(email))
        //    {
        //        query = query.Where(x => x.Client.Person.Email.ToLower().Contains(email.ToLower()));
        //    }

        //    if (active == ActiveType.Active)
        //    {
        //        query = query.Where(x => x.ValidUntil > DateTime.Now && x.LinkUsages.Count() < x.MaxUsages);
        //    }
        //    else if (active == ActiveType.NotActive)
        //    {
        //        query = query.Where(x => x.ValidUntil < DateTime.Now || x.LinkUsages.Count() >= x.MaxUsages);
        //    }

        //    return query.OrderBy(x => x.Id).ToList();
        //}

        //public List<LinkView> Search(int ebookId)
        //{
        //    var param1 = new OracleParameter("P_ERAAMAT_ID", OracleDbType.Int32, ebookId, ParameterDirection.Input);
        //    var param2 = new OracleParameter("CURSOR_", OracleDbType.RefCursor, ParameterDirection.Output);

        //    return _context.Database.SqlQuery<LinkView>(
        //        "BEGIN SEARCH_LINK(:P_ERAAMAT_ID, :CURSOR_); end;",
        //        param1, param2).ToList();
        //}
    }
}