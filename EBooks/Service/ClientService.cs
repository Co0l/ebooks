﻿using EBooks.Domain;
using EBooks.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Service
{
    public class ClientService : Repository<Client>
    {

        public ClientService(EBookContext context) : base(context)
        {

        }

        public List<Client> GetAvailableClients()
        {
            return this.Table.Where(x => x.RulesAccepted).OrderBy(x => x.Person.Firstname).ToList();
        }
    }
}