﻿using EBooks.Domain;
using EBooks.Domain.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Service.Views
{
    public class LinkUsageViewService : Repository<LinkUsageView>
    {

        public LinkUsageViewService(EBookContext context) : base(context)
        {

        }

        public IList<LinkUsageView> GetByLinkId(int linkId)
        {
            if (linkId == 0)
                throw new ArgumentNullException("linkId");

            return Table.Where(x => x.LinkId == linkId).ToList();
        }
    }
}