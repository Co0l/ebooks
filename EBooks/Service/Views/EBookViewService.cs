﻿using EBooks.Domain;
using EBooks.Domain.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Service.Views
{
    public class EBookViewService : Repository<EBookView>
    {

        public EBookViewService(EBookContext context) : base(context)
        {

        }

        public List<EBookView> Search(string productCode = null, string name = null, string author = null)
        {
            var query = this.Table;

            if (!string.IsNullOrWhiteSpace(productCode))
            {
                query = query.Where(x => x.ProductCode.ToLower().Contains(productCode.ToLower()));
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query.Where(x => x.Name.ToLower().Contains(name.ToLower()));
            }

            if (!string.IsNullOrWhiteSpace(author))
            {
                query = query.Where(x => x.Author.ToLower().Contains(author.ToLower()));
            }

            return query.OrderBy(x => x.Id).ToList();
        }
    }
}