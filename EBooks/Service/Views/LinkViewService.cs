﻿using EBooks.Domain;
using EBooks.Domain.View;
using EBooks.Enums;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace EBooks.Service.Views
{
    public class LinkViewService : Repository<LinkView>
    {

        public LinkViewService(EBookContext context) : base(context)
        {

        }

        public List<LinkView> Search(string email, int ebookId, int active)
        {
            var emailParam = new OracleParameter("P_EMAIL", OracleDbType.Varchar2, 254, email, ParameterDirection.Input);
            var ebookParam = new OracleParameter("P_ERAAMAT_ID", OracleDbType.Int32, 10, ebookId, ParameterDirection.Input);
            var activeParam = new OracleParameter("P_AKTIIVNE", OracleDbType.Int32, 1, active, ParameterDirection.Input);
            var result = new OracleParameter("CURSOR_", OracleDbType.RefCursor, ParameterDirection.Output);

            return _context.Database.SqlQuery<LinkView.SPObject>(
                "BEGIN C##TUD3.T167023.LINKIDE_OTSING(:P_EMAIL, :P_ERAAMAT_ID, :P_AKTIIVNE, :CURSOR_); end;",
                emailParam, ebookParam, activeParam, result).Select(x => x.ToLinkView()).ToList();


            //var idParam = new OracleParameter
            //{
            //    ParameterName = "P_ERAAMAT_ID",
            //    Value = ebookId,
            //    DbType
            //};
            //return _context.Database.SqlQuery<Link>(
            //"exec TEST(:P_ERAAMAT_ID) ", idParam).ToList<Link>();
        }
    }
}