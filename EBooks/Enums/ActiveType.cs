﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Enums
{
    public enum ActiveType
    {
        Any = 0,
        Active = 1,
        NotActive = 2,
    }
}