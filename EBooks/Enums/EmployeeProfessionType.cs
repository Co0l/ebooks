﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Enums
{
    public enum ProfessionType
    {
        Administrator = 1,
        EBookManager = 2,
        Vendor = 3
    }
}