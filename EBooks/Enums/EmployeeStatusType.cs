﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Enums
{
    public enum EmployeeStatusType
    {
        TestPeriod = 1,
        Contract = 2,
        OnVacation = 3,
        Sick = 4,
        ContractCanceled = 5,
        ContractCanceledByPerson = 6,
        Fired = 7
    }
}