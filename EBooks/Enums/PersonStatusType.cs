﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Enums
{
    public enum PersonStatusType
    {
        Alive = 1,
        Dead = 2
    }
}