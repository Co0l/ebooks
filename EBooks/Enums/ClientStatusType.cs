﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Enums
{
    public enum ClientStatusType
    {
        Inactive = 1,
        Active = 2,
        Blocked = 3
    }
}