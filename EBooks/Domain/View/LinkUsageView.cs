﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.View
{
    public class LinkUsageView : Entity
    {
        public int LinkId { get; set; }
        public string FailName { get; set; }
        public DateTime UsedOn { get; set; }
        public string Ip { get; set; }
        public string UserAgent { get; set; }
    }
}