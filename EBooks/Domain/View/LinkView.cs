﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.View
{
    public class LinkView : Entity
    {

        public int EBookId { get; set; }

        public string EBookCode { get; set; }

        public string EBookName { get; set; }

        public string ClientEmail { get; set; }

        public int MaxUsages { get; set; }

        public int LeftUsages { get; set; }

        public DateTime ValidUntil { get; set; }

        public class SPObject
        {
            public int LINK_ID { get; set; }
            public int ERAAMAT_ID { get; set; }
            public string ERAAMATU_KOOD { get; set; }
            public string PEALKIRI { get; set; }
            public string EMAIL { get; set; }
            public int ALLALAADMISE_ARV { get; set; }
            public int SAADAVAL { get; set; }
            public DateTime KEHTIV_KUNI { get; set; }

            public LinkView ToLinkView()
            {
                return new LinkView()
                {
                    Id = LINK_ID,
                    EBookId = ERAAMAT_ID,
                    EBookCode = ERAAMATU_KOOD,
                    EBookName = PEALKIRI,
                    ClientEmail = EMAIL,
                    MaxUsages = ALLALAADMISE_ARV,
                    LeftUsages = SAADAVAL,
                    ValidUntil = KEHTIV_KUNI
                };
            }
        }

    }
}