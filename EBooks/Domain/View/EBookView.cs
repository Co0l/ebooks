﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.View
{
    public class EBookView : Entity
    {
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public int DownloadCount { get; set; }
        public int DownloadDays { get; set; }
        public DateTime CreatedOn { get; set; }
        public int NumberOfDownloads { get; set; }
        public int NumberOfLinks { get; set; }
    }
}