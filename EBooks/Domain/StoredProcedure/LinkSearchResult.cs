﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.StoredProcedure
{
    public class LinkSearchResult
    {

        public int Id { get; set; }

        public string EBookCode { get; set; }

        public string EBookName { get; set; }

        public string ClientEmail { get; set; }

        public int MaxUsages { get; set; }

        public int Usages { get; set; }

        public DateTime ValidUntil { get; set; }
    }
}