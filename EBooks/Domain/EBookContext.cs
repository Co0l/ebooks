﻿using EBooks.Mapping.Catalog;
using EBooks.Models;
using EBooks.Domain.Catalog;
using EBooks.Domain.Users;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using System.Web;
using EBooks.Domain.View;

namespace EBooks.Domain
{
    public class EBookContext : DbContext
    {

        public EBookContext() : base("EBookContext") 
        {
            Database.SetInitializer<EBookContext>(new CreateDatabaseIfNotExists<EBookContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("C##TUD3");

            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
            .Where(type => !String.IsNullOrEmpty(type.Namespace))
            .Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
                type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }


            //modelBuilder.Configurations.Add(new EBookCategoryMap());


            //base.OnModelCreating(modelBuilder);
        }

        public IDbSet<EBookView> ViewEBooks { get; set; }
        public IDbSet<LinkView> ViewLinks { get; set; }
        public IDbSet<LinkUsageView> ViewLinkUsages { get; set; }


        public IDbSet<EBook> EBooks { get; set; }
        public IDbSet<Link> Links { get; set; }
        public IDbSet<LinkUsage> LinkUsages { get; set; }
        public IDbSet<EBookCategory> EBookCategories { get; set; }
        public IDbSet<EBookCategoryMapping> EBookCategoryMappings { get; set; }
        public IDbSet<EBookFile> EBookFiles { get; set; }
        public IDbSet<File> Files { get; set; }
        public IDbSet<Person> Persons { get; set; }
        public IDbSet<PersonStatus> PersonStatuses { get; set; }
        public IDbSet<Client> Clients { get; set; }
        public IDbSet<ClientStatus> ClientStatuses { get; set; }
        public IDbSet<Employee> Employees { get; set; }
        public IDbSet<EmployeeStatus> EmployeeStatuses { get; set; }
        public IDbSet<Profession> Professions { get; set; }
        public IDbSet<EmployeeProfession> EmployeeProfessions { get; set; }
    }
}