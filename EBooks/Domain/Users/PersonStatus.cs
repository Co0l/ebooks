﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Users
{
    public class PersonStatus : Entity
    {

        public string Name { get; set; }

        public string Description { get; set; }

    }
}