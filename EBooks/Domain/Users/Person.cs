﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Users
{
    public class Person : Entity
    {

        public int PersonStatusId { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public DateTime RegisteredOn { get; set; } = DateTime.Now;

        public virtual PersonStatus PersonStatus { get; set; }


    }
}