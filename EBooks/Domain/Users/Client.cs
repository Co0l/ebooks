﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Users
{
    public class Client : Entity
    {

        public int PersonId { get; set; }

        public int ClientStatusId { get; set; }

        public bool RulesAccepted { get; set; }

        public virtual ClientStatus ClientStatus { get; set; }

        public virtual Person Person { get; set; }
    }
}