﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Users
{
    public class Employee : Entity
    {

        public int PersonId { get; set; }

        public int EmployeeStatusId { get; set; }

        public virtual Person Person { get; set; }

        public virtual EmployeeStatus EmployeeStatus { get; set; }

        public virtual ICollection<EmployeeProfession> EmployeeProfessions { get; set; }

    }
}