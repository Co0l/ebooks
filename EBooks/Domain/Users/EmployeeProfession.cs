﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Users
{
    public class EmployeeProfession : Entity
    {

        public int EmployeeId { get; set; }

        public int ProfessionId { get; set; }

        public virtual Profession Profession { get; set; }

        public virtual Employee Employee { get; set; }
    }
}