﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EBooks.Domain.Catalog;
using EBooks.Domain.Users;

namespace EBooks.Domain.Catalog
{
    public class Link : Entity
    {

        public int ClientId { get; set; }

        public int EmployeeId { get; set; }

        public int EBookId { get; set; }

        public int MaxUsages { get; set; }

        public DateTime ValidUntil { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.Now;

        public virtual EBook EBook { get; set; }

        public virtual ICollection<LinkUsage> LinkUsages { get; set; } 

        public virtual Client Client { get; set; }

        public virtual Employee Employee { get; set; }
    }
}