﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Catalog
{
    public class LinkUsage : Entity
    {
        public int LinkId { get; set; }

        public int EBookFileId { get; set; }

        public DateTime UsedOn { get; set; } = DateTime.Now;

        public string Ip { get; set; }

        public string UserAgent { get; set; }

        public virtual Link Link { get; set; }

        public virtual EBookFile EBookFile { get; set; }
    }
}