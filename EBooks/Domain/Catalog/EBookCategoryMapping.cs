﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Catalog
{
    public class EBookCategoryMapping : Entity
    {

        public int EBookId { get; set; }

        public int EBookCategoryId { get; set; }

        public virtual EBook EBook { get; set; }

        public virtual EBookCategory EBookCategory { get; set; }
    }
}