﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Catalog
{
    public class EBookCategory : Entity
    {

        public string Name { get; set; }

        public string Description { get; set; }

    }
}