﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Catalog
{

    public class File : Entity
    {

        public string Path { get; set; }

        public string FileName { get; set; }

        public int Size { get; set; }

        public bool Outdated { get; set; }

    }
}