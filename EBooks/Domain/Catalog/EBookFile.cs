﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Catalog
{
    public class EBookFile : Entity
    {
        public int EBookId { get; set; }

        public int FileId { get; set; }

        public virtual EBook EBook { get; set; }

        public virtual File File { get; set; }

    }
}