﻿using EBooks.Domain.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EBooks.Domain.Catalog
{
    public class EBook : Entity
    {

        public int EmployeeId { get; set; }

        public string ProductCode { get; set; }

        public string Name { get; set; }

        public string SubTitle { get; set; }

        public string Description { get; set; }

        public string Author { get; set; }

        public int DownloadCount { get; set; }

        public int DownloadDays { get; set; }

        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Link> Links { get; set; }

        public virtual ICollection<EBookCategoryMapping> EBookCategoryMappings { get; set; }

        public virtual ICollection<EBookFile> EBookFiles { get; set; }

        public virtual Employee Employee { get; set; }

        public EBook()
        {
            DownloadCount = 3;
            DownloadDays = 7;
            CreatedOn = DateTime.Now;
        }

    }
}