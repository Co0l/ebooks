﻿using AutoMapper;
using EBooks.Domain.Catalog;
using EBooks.Domain.Users;
using EBooks.Domain.View;
using EBooks.Models.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBooks.App_Start
{
    public class AutoMapperConfig
    {

        public static void RegisterMaps()
        {
            Mapper.Initialize(cfg => Register(cfg));
        }

        public static void Register(IMapperConfiguration cfg)
        {
            cfg.CreateMap<EBook, EBookModel>();
            cfg.CreateMap<EBookView, EBookModel>();

            cfg.CreateMap<EBookModel, EBook>();

            cfg.CreateMap<Link, LinkListModel>()
                .ForMember(x => x.Usages, m => m.MapFrom(src => src.LinkUsages.Count))
                .ForMember(x => x.EBookCode, m => m.MapFrom(src => src.EBook.ProductCode))
                .ForMember(x => x.EBookName, m => m.MapFrom(src => src.EBook.Name))
                .ForMember(x => x.ClientEmail, m => m.MapFrom(src => src.Client.Person.Email))
                .ForMember(x => x.LeftUsages, m => m.MapFrom(src => (src.MaxUsages - src.LinkUsages.Count < 0) ? 0 : src.MaxUsages - src.LinkUsages.Count));

            cfg.CreateMap<LinkView, LinkListModel>();

            cfg.CreateMap<Link, LinkModel>()
                .ForMember(x => x.Client, m => m.MapFrom(src => Mapper.Map<ClientModel>(src.Client)))
                .ForMember(x => x.Employee, m => m.MapFrom(src => Mapper.Map<EmployeeModel>(src.Employee)))
                .ForMember(x => x.EBook, m => m.MapFrom(src => Mapper.Map<EBookModel>(src.EBook)))
                .ForMember(x => x.LinkUsages, m => m.Ignore());

            cfg.CreateMap<LinkModel, Link>()
                .ForMember(x => x.Client, m => m.Ignore())
                .ForMember(x => x.Employee, m => m.Ignore())
                .ForMember(x => x.LinkUsages, m => m.Ignore())
                .ForMember(x => x.EBook, m => m.Ignore())
                .ForAllMembers(m => m.UseDestinationValue());



            cfg.CreateMap<LinkUsage, LinkUsageListModel>()
                .ForMember(x => x.FileName, m => m.MapFrom(src => src.EBookFile.File.FileName));

            cfg.CreateMap<Employee, EmployeeModel>()
                .ForMember(x => x.Email, m => m.MapFrom(src => src.Person.Email))
                .ForMember(x => x.Address, m => m.MapFrom(src => src.Person.Address))
                .ForMember(x => x.Firstname, m => m.MapFrom(src => src.Person.Firstname))
                .ForMember(x => x.Lastname, m => m.MapFrom(src => src.Person.Lastname))
                .ForMember(x => x.Phone, m => m.MapFrom(src => src.Person.Phone));

            cfg.CreateMap<Client, ClientModel>()
                .ForMember(x => x.Email, m => m.MapFrom(src => src.Person.Email))
                .ForMember(x => x.Address, m => m.MapFrom(src => src.Person.Address))
                .ForMember(x => x.Firstname, m => m.MapFrom(src => src.Person.Firstname))
                .ForMember(x => x.Lastname, m => m.MapFrom(src => src.Person.Lastname))
                .ForMember(x => x.Phone, m => m.MapFrom(src => src.Person.Phone));


        }

    }
}