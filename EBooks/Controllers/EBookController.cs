﻿using AutoMapper;
using EBooks.Domain.Catalog;
using EBooks.Models.Catalog;
using EBooks.Service;
using EBooks.Service.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBooks.Controllers
{
    [Authorize]
    public class EBookController : CommonController
    {

        private readonly EBookService _eBookService;
        private readonly EBookViewService _eBookViewService;

        public EBookController()
        {
            _eBookService = new EBookService(_context);
            _eBookViewService = new EBookViewService(_context);
        }

        public ActionResult Index()
        {
            var model = new EBookQueryModel();
            model.EBooks = _eBookViewService.GetAll().Select(x => Mapper.Map<EBookModel>(x)).ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(EBookQueryModel model)
        {
            model.EBooks = _eBookViewService.Search(
                                productCode: model.SearchByProductCode,
                                author: model.SearchByAuthor,
                                name: model.SearchByName)
                        .Select(x => Mapper.Map<EBookModel>(x)).ToList();
            return View("Index", model);
        }

        public ActionResult Details(int id)
        {
            var ebook = _eBookService.GetById(id);
            if (ebook == null)
                return HttpNotFound();

            var model = Mapper.Map<EBookModel>(ebook);

            return View(model);
        }

    }
}