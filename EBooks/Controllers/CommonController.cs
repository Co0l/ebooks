﻿using EBooks.Domain;
using EBooks.Domain.Users;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System;
using System.Web.Routing;
using System.Web;
using System.Web.WebPages;
using EBooks.Service;
using EBooks.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;

namespace EBooks.Controllers
{
    public class CommonController : Controller
    {
        protected readonly EBookContext _context;

        public CommonController()
        {
            _context = new EBookContext();
        }

        public Employee CurrentUser
        {
            get
            {
                if (User.Identity.IsAuthenticated)
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var email = claimsIdentity.FindFirst(ClaimTypes.Email).Value;
                    return _context.Employees.FirstOrDefault(x => x.Person.Email == email);
                }
                return null;
            }
        }
        
    }
}