﻿using AutoMapper;
using EBooks.Domain.Catalog;
using EBooks.Enums;
using EBooks.Models.Catalog;
using EBooks.Service;
using EBooks.Service.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EBooks.Controllers
{
    [Authorize]
    public class LinkController : CommonController
    {

        private readonly EBookService _eBookService;
        private readonly LinkService _linkService;
        private readonly LinkViewService _linkViewService;
        private readonly LinkUsageViewService _linkUsageViewService;
        private readonly ClientService _clientService;

        public LinkController()
        {
            _eBookService = new EBookService(_context);
            _linkService = new LinkService(_context);
            _linkViewService = new LinkViewService(_context);
            _linkUsageViewService = new LinkUsageViewService(_context);
            _clientService = new ClientService(_context);
        }

        public ActionResult Index()
        {
            var model = new LinkQueryModel();
            model.Links = _linkViewService.GetAll().Select(x => Mapper.Map<LinkListModel>(x)).ToList();
            model.AvailableEBooks = GetEBookSelectList();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(LinkQueryModel model)
        {
            if (model == null)
                throw new NullReferenceException("model is null");

            //model.Links = _linkService.Search(model.SearchByEmail, model.SerachByEBookId, (ActiveType) model.SelectedActiveId).Select(x => Mapper.Map<LinkListModel>(x)).ToList();
            model.Links = _linkViewService.Search(model.SearchByEmail, model.SearchByEBookId, model.SelectedActiveId).Select(x => Mapper.Map<LinkListModel>(x)).ToList(); 
            model.AvailableEBooks = GetEBookSelectList();
            return View(model);
        }

        public ActionResult Create(int eBookId = 0)
        {
            var model = new LinkModel();
            model.EmployeeId = CurrentUser.Id;
            model.AvailableEBooks = GetEBookSelectList(false);
            model.AvailableClients = GetClientSelectList();
            model.CreatedOn = DateTime.Now;
            
            if (eBookId > 0)
            {
                var book = _eBookService.GetById(eBookId);
                if (book != null)
                {
                    model.EBookId = book.Id;
                    model.ValidUntil = DateTime.Now.AddDays(book.DownloadDays);
                    model.MaxUsages = book.DownloadCount;
                }
            }
           
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(LinkModel model)
        {
            if (ModelState.IsValid)
            {
                var link = Mapper.Map<Link>(model);
                //_linkService.Insert(link);
                //return RedirectToAction("Edit", new { id = linkId, success = true });

                _linkService.Add(link);
                return RedirectToAction("Index");
            }

            model.AvailableEBooks = GetEBookSelectList(false);
            model.AvailableClients = GetClientSelectList();
            return View(model);
        }


        public ActionResult Edit(int id, bool success = false) 
        {
            var link = _linkService.GetById(id);
            if (link == null)
                throw new Exception($"Link '{id}' ei leitud ");

            var model = Mapper.Map<LinkModel>(link);
            model.LinkUsages = GetLinkUsages(link);
            model.AvailableEBooks = GetEBookSelectList(false);
            model.AvailableClients = GetClientSelectList();
            if (success)
            {
                model.SuccessMessage = "Lingi andmed salvestatud";
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(LinkModel model)
        {
            var link = _linkService.GetById(model.Id);
            if (link == null)
                throw new Exception($"Link '{model.Id}' ei leitud ");

            if (ModelState.IsValid)
            {
                link = Mapper.Map(model, link);
                _linkService.Update(link);
                return RedirectToAction("Edit", new { id = model.Id, success = true});
            }

            model.LinkUsages = GetLinkUsages(link);
            model.AvailableEBooks = GetEBookSelectList(false);
            model.AvailableClients = GetClientSelectList();
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var link = _linkService.GetById(id);
            if (link == null)
                throw new Exception($"Link '{id}' ei leitud ");

            //_linkService.Delete(link);
            _linkService.Remove(id);
            return RedirectToAction("Index");
        }

        #region helpers

        private List<LinkUsageListModel> GetLinkUsages(Link link)
        {
            if (link == null)
                return new List<LinkUsageListModel>();

            var linkUsages = _linkUsageViewService.GetByLinkId(link.Id);

            return linkUsages.Select(x => new LinkUsageListModel()
            {
               Id = x.Id,
               FileName = x.FailName,
               Ip = x.Ip,
               UsedOn = x.UsedOn,
               UserAgent = x.UserAgent 
            }).ToList();

            //return link.LinkUsages.Select(x => Mapper.Map<LinkUsageListModel>(x)).ToList();
        }

        private IList<SelectListItem> GetEBookSelectList(bool includeAll = true)
        {
            var list = new List<SelectListItem>();
            if (includeAll)
            {
                list.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "Kõik"
                });
            }
                
            foreach (var book in _eBookService.GetAll())
            {
                list.Add(new SelectListItem()
                {
                    Value = book.Id.ToString(),
                    Text = $"{book.Name} ({book.ProductCode})"
                });
            }
            return list;
        }

        private IList<SelectListItem> GetClientSelectList()
        {
            var list = new List<SelectListItem>();
            foreach (var client in _clientService.GetAvailableClients())
            {
                list.Add(new SelectListItem()
                {
                    Value = client.Id.ToString(),
                    Text = $"{client.Person.Firstname} {client.Person.Lastname}, {client.Person.Email}"
                });
            }
            return list;
        }

        #endregion
    }
}