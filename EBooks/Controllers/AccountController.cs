﻿using EBooks.Infrastructure;
using EBooks.Models.Account;
using EBooks.Service;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace EBooks.Controllers
{

    [Authorize]
    public class AccountController : CommonController
    {

        private readonly UserService _userService;

        public AccountController()
        {
            _userService = new UserService(_context);
        }

        public ActionResult Index()
        {
            var ctx = Request.GetOwinContext();

            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl = "/")
        {
            return View(new LoginModel());
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl = "/")
        {
            if (ModelState.IsValid)
            {
                var user = _userService.ValidateUser(model.Email, model.Password);
                if (user != null)
                {
                    if (_userService.IsActiveVendor(user))
                    {
                        var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                            new Claim(ClaimTypes.Name, $"{user.Firstname} {user.Lastname}"),
                            new Claim(ClaimTypes.Email, user.Email),
                        },
                        DefaultAuthenticationTypes.ApplicationCookie);

                        var ctx = Request.GetOwinContext();
                        var authManager = ctx.Authentication;

                        authManager.SignIn(identity);

                        return RedirectToLocal(returnUrl);
                    }
                    ModelState.AddModelError(string.Empty, "Juurdepääs keelatud!");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Kasutaja tuvastamine ebaõnnestus");
                }

            }

            return View(model);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut();
            return RedirectToAction("Login");
        }

        

    }
}